#!/bin/bash
# roles/openssl/files/install.sh

yum -y install gcc >> /home/dont/install_gcc.log

wget https://www.openssl.org/source/openssl-1.1.1a.tar.gz -O /tmp/openssl-1.1.1a.tar.gz

cd /tmp
tar -xvzf openssl-1.1.1a.tar.gz

cd openssl-1.1.1a

#./config --prefix=/usr         \
#  --openssldir=/etc/ssl \
#  --libdir=lib          \
#  shared                \
#  zlib-dynamic

./config >> /home/dont/openssl_config.log
make >> /home/dont/openssl_makee.log
make install >> /home/dont/openssl_make_install.log

#make clean

mv /usr/bin/openssl /root/openssl_old
ln -s /usr/local/bin/openssl /usr/bin/openssl

echo "/usr/local/lib64" >> /etc/ld.so.conf

cd ..

ldconfig

rm -rf openssl-1.1.1a.tar.gz openssl-1.1.1a/
