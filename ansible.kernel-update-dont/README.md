# Ansible Role: update curl with city_fan repo

## Requirements

None.

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

	 city_fan_repo_url: "http://www.city-fan.org/ftp/contrib/yum-repo/city-fan.org-release-2-1.rhel{{ ansible_distribution_major_version }}.noarch.rpm"

The URL from which the City)fan repo `.rpm` will be downloaded and installed.

	city_fan_repo_gpg_key_url: "http://www.city-fan.org/ftp/contrib/yum-repo/CITY-FAN.ORG-GPG-KEY"
	

## Dependencies

None.


